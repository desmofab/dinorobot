
export class HealthBar {
  bar = <Phaser.GameObjects.Graphics>{}
  x: number
  y: number
  life: number
  mainColor: number
  private points: number

  constructor (scene: Phaser.Scene, x: number, y: number, life?: number, mainColor?: number) {
    this.bar = new Phaser.GameObjects.Graphics(scene)
    this.x = x;
    this.y = y;
    this.life = life || 100;
    this.points = 76 / 100;
    this.mainColor = mainColor || 0x00ff00
    this.draw()
    scene.add.existing(this.bar)
  }

  decrease (amount: number) {
    this.life -= amount
    if (this.life < 0)
    {
        this.life = 0
    }
    this.draw()
    return (this.life === 0)
  }

  draw () {
    this.bar.clear()
    // Borders
/*     this.bar.fillStyle(0x555555)
    this.bar.fillRect(this.x, this.y, 3, 78) */

    // BG empty
    this.bar.fillStyle(0xffffff)
    this.bar.fillRect(this.x + 1, this.y + 1, 3, 76)

    if (this.life < 50) {
        this.bar.fillStyle(0xff0000)
    }
    else{
        this.bar.fillStyle(this.mainColor)
    }

    var d = Math.floor((this.points * this.life) * -1)
    this.bar.fillRect(this.x + 1, this.y + 1 +76, 3, d)
  }

  destroy() {
    this.bar.destroy()
  }

}
