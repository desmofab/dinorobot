export class Starfield extends Phaser.GameObjects.TileSprite {
  constructor(scene: Phaser.Scene, x: number, y: number, width: number, height: number) {
    super(scene, x, y, width, height, 'starfield')
    scene.add.existing(this)
  }

  override update(): Starfield {
    this.tilePositionY -= 2
    return this
  }
}
