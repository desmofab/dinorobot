import { Injectable } from '@angular/core';
import { Final } from '@scenes/final';
import { GameOver } from '@scenes/gameover';
import { IntroScene } from '@scenes/intro/intro';
import { Level1 } from '@scenes/level1';
import { Level2 } from '@scenes/level2';
import * as Phaser from 'phaser';

@Injectable({
  providedIn: 'root'
})
export class GameService {
  title = 'dinorobot';
  private _game = <Phaser.Game>{};
  private _config = <Phaser.Types.Core.GameConfig>{};

  get config(): Phaser.Types.Core.GameConfig {
    return this._config;
  }
  set config(newConfig: Phaser.Types.Core.GameConfig) {
    this._config = newConfig;
  }

  get game(): Phaser.Game {
    return this._game;
  }
  set game(newGame: Phaser.Game) {
    this._game = newGame;
  }

  get baseConfig(): Phaser.Types.Core.GameConfig {
    return {
      type: Phaser.AUTO,
      scale: {
        mode: Phaser.Scale.FIT,
        autoCenter: Phaser.Scale.CENTER_BOTH,
        width: 800,
        height: 600
      },
      physics: {
        default: 'arcade',
        arcade: {
            gravity: { y: 0 },
            debug: false
        }
      },
      input: {
        activePointers: 3,
      },
      scene: [IntroScene, Level1, Level2, Final, GameOver]
    }
  }

  constructor() {
    this.config = this.baseConfig;
  }

  createGame(setConfig?: Phaser.Types.Core.GameConfig): Phaser.Game {
    if (setConfig) {
      this.config = setConfig;
    }
    this.game = new Phaser.Game(this.config);
    return this.game;
  }
}
