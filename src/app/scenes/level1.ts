import { Eye1, Eyes1 } from '@bads/eyes1/eye1'
import { Eye1Bullet } from '@bads/eyes1/eye1-bullet'
import { Level1Completed } from '@scenes/intro/texts'
import { Level } from '@scenes/level.model'
import { ShipBullet } from '@ship/bullet'
import { Ship } from '@ship/ship'
import * as Phaser from 'phaser'
import { EaseMoveTo } from 'phaser3-rex-plugins/plugins/easemove.js'
import RoundRectangle from 'phaser3-rex-plugins/plugins/roundrectangle'
import { TextBox } from 'phaser3-rex-plugins/templates/ui/ui-components'
import { Starfield } from 'src/app/background/starfield'


export class Level1 extends Phaser.Scene implements Level {
  starfield = <Starfield>{}
  ship = <Ship>{}
  eyes1 = <Eyes1>{}
  explosions = <Phaser.GameObjects.Group>{}

  get isLevelCompleted(): boolean {
    for (const eye of this.eyes1.getChildren()) {
      if (eye.active) {
        return false
      }
    }
    return true
  }

  constructor () {
    super('level1')
  }

  preload() {
    this.load.setPath('assets/')
    this.load.image('starfield', 'backgrounds/starfield.jpg')
    this.load.image('ship', 'ships/fede.png')
    this.load.image('blu', 'particles/blue.png')
    this.load.image('ship-bullet', 'ships/bullet.png')
    this.load.image('bad1', 'bads/bad1.png')
    this.load.spritesheet('eye1-bullet', 'bads/eye1-bullet.png', {
      frameWidth: 24,
      frameHeight: 24,
      startFrame: 0,
      endFrame: 14
    })
    this.load.spritesheet('kaboom', 'particles/explode.png', {
      frameWidth: 128,
      frameHeight: 128,
      startFrame: 0,
      endFrame: 15
    })
    this.load.spritesheet('kaboom2', 'particles/explode2.png', {
      frameWidth: 64,
      frameHeight: 64,
      startFrame: 0,
      endFrame: 22
    })

    //this.load.audio('music', 'audio/music.mp3')
    this.load.audio('audio_laser', 'audio/laser.mp3')
    this.load.audio('audio_explosion', 'audio/explosion.mp3')
    this.load.audio('ship_explosion', 'audio/explosion5.mp3')
    this.load.audio('audio_hit', 'audio/hit2.mp3')
    this.load.audio('ship-hit', 'audio/ship-hit.mp3')
    this.load.audio('audio_engine', 'audio/afterburner.mp3')
  }

  create(data: any) {
    //this.sound.add('music').play({loop: true, volume: 0.5})

    this.starfield = new Starfield(this, 400, 300, 800, 600)
    this.ship = new Ship(this, 400, 500).readyToPlay()
    this.eyes1 = new Eyes1(this).fireOrder(this.ship)

    //Audio
    this.sound.add('audio_explosion')
    this.sound.add('audio_hit')

    //this.physics.add.collider(this.ship,this.aliens, <ArcadePhysicsCallback>this.collideShipEnemies)
    this.physics.add.collider(this.ship.bullets, this.eyes1, <ArcadePhysicsCallback>this.hitEnemy)
    this.physics.add.collider(this.ship, this.eyes1.bullets, <ArcadePhysicsCallback>this.hitShip)

    const an = this.anims.create({
      key: 'eye1-bullet',
      duration: 900,
      frames: this.anims.generateFrameNames('eye1-bullet'),
      repeat: -1,
      yoyo: true
    })
    this.anims.create({
      key: 'kaboom',
      duration: 900,
      frames: this.anims.generateFrameNames('kaboom'),
      repeat: 0,
      showOnStart: true,
      hideOnComplete: true
    })
    this.anims.create({
      key: 'kaboom2',
      duration: 900,
      frames: this.anims.generateFrameNames('kaboom2'),
      repeat: 0,
      showOnStart: true,
      hideOnComplete: true
    })
  }

  override update(time: number, delta: number) {
    this.starfield.update()
    this.ship.update()
    this.eyes1.update()
  }

  collideShipEnemies(ship: Ship, alien: Eye1) {
    ship.hit()
  }

  hitEnemy(bullet: ShipBullet, alien: Eye1) {
    bullet.destroy()
    alien.hit()
  }

  hitShip(ship: Ship, bullet: Eye1Bullet) {
    bullet.destroy()
    ship.hit()
  }

  levelCompleted() {
    if (!this.isLevelCompleted) {
      return
    }
    EaseMoveTo(this.ship, 1000, 400, 200, 'Linear')
    this.ship.setScale(1).setVelocity(0, 0).life.destroy()
    this.ship.removeJoystick()
    this.ship.fireButton.destroy()

    const background = new RoundRectangle(this,0, 0, 2, 2, 20, 0x000000).setStrokeStyle(2, 0xffffff)
    this.add.existing(background)
    const icon = this.add.image(0, 0, 'spaceinveder')
    const text = this.add.bitmapText(0, 0, 'pixel').setFontSize(20).setMaxWidth(550)

    const textBox = new TextBox(this, {
      x: 50,
      y: 500,
      anchor: {
        centerX: '50%+0',
      },
      align: 'center',
      background,
      icon,
      page: {
        pageBreak: '\n',
        maxLines: 3
      },
      text,
      action: this.add.image(0, 0, 'nextPage').setTint(0xffffff).setVisible(false),
      space: {
          left: 20,
          right: 20,
          top: 20,
          bottom: 20,
          icon: 10,
          text: 10,
      }
    })

    textBox.setOrigin(0).layout().setInteractive()
      .on('pointerdown', () => {
        if (textBox.isLastPage) { // Jump to next scene
          this.scene.start('level2')
          return
        }
        const icon = <Phaser.GameObjects.Image>textBox.getElement('action')
        icon.setVisible(false)
        textBox.resetChildVisibleState(icon)
        if (textBox.isTyping) {
          textBox.stop(true)
        } else {
          textBox.typeNextPage()
        }
      })
      .on('pageend', () => {
        if (textBox.isLastPage) {
            return
        }
        const icon = <Phaser.GameObjects.Image>textBox.getElement('action')
        icon.setVisible(true)
        textBox.resetChildVisibleState(icon)
        icon.y -= 30
        const tween = this.tweens.add({
            targets: icon,
            y: '+=30', // '+=100'
            ease: 'Bounce', // 'Cubic', 'Elastic', 'Bounce', 'Back'
            duration: 500,
            repeat: 0, // -1: infinity
            yoyo: false
        })
      })

    this.add.existing(textBox).start(Level1Completed, 50)
  }

}
