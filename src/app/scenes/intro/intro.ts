import { IntroText } from '@scenes/intro/texts'
import { Level } from '@scenes/level.model'
import { Ship } from '@ship/ship'
import * as Phaser from 'phaser'
import { RoundRectangle, TextBox } from 'phaser3-rex-plugins/templates/ui/ui-components.js'
import { Starfield } from 'src/app/background/starfield'

const COLOR_PRIMARY = 0x000000
const COLOR_LIGHT = 0xffffff

export class IntroScene extends Phaser.Scene implements Level {
  starfield = <Starfield>{}
  ship = <Ship>{}
  textBox = <TextBox>{}

  get isLevelCompleted(): boolean {
    return false
  }

  constructor () {
    super('intro')
  }

  preload() {
    this.load.setPath('assets/')
    this.load.image('starfield', 'backgrounds/starfield.jpg')
    this.load.image('ship', 'ships/fede.png')
    this.load.image('blu', 'particles/blue.png')
    this.load.audio('music', 'audio/music.mp3')
    this.load.bitmapFont('pixel', 'fonts/pixel.png', 'fonts/pixel.xml')
    this.load.image('spaceinveder', 'ui/spaceinveder.png')
    this.load.image('nextPage', 'ui/arrow-down-left.png')
  }

  create(data: any) {
    this.sound.add('music').play({loop: true, volume: 0.5})
    this.starfield = new Starfield(this, 400, 300, 800, 600)
    this.ship = new Ship(this, 400, 200).setScale(1).createAfterBurner({x: 0, y: 100})

    const background = new RoundRectangle(this,0, 0, 2, 2, 20, COLOR_PRIMARY).setStrokeStyle(2, COLOR_LIGHT)
    this.add.existing(background)
    const icon = this.add.image(0, 0, 'spaceinveder')
    const text = this.add.bitmapText(0, 0, 'pixel').setFontSize(20).setMaxWidth(550)

    this.textBox = new TextBox(this, {
      x: 50,
      y: 500,
      anchor: {
        centerX: '50%+0',
      },
      align: 'center',
      background,
      icon,
      page: {
        pageBreak: '\n',
        maxLines: 3
      },
      text,
      action: this.add.image(0, 0, 'nextPage').setTint(COLOR_LIGHT).setVisible(false),
      space: {
          left: 20,
          right: 20,
          top: 20,
          bottom: 20,
          icon: 10,
          text: 10,
      }
    })

    this.textBox.setOrigin(0).layout().setInteractive()
      .on('pointerdown', () => {
        if (this.textBox.isLastPage) { // Jump to next scene
          this.scene.start('level1')
          return
        }
        const icon = <Phaser.GameObjects.Image>this.textBox.getElement('action')
        icon.setVisible(false)
        this.textBox.resetChildVisibleState(icon)
        if (this.textBox.isTyping) {
          this.textBox.stop(true)
        } else {
          this.textBox.typeNextPage()
        }
      })
      .on('pageend', () => {
        if (this.textBox.isLastPage) {
            return
        }
        const icon = <Phaser.GameObjects.Image>this.textBox.getElement('action')
        icon.setVisible(true)
        this.textBox.resetChildVisibleState(icon)
        icon.y -= 30
        const tween = this.tweens.add({
            targets: icon,
            y: '+=30', // '+=100'
            ease: 'Bounce', // 'Cubic', 'Elastic', 'Bounce', 'Back'
            duration: 500,
            repeat: 0, // -1: infinity
            yoyo: false
        })
      })

    this.add.existing(this.textBox).start(IntroText, 50)
  }

  override update(time: number, delta: number) {
    this.starfield.update()
  }

  levelCompleted() {}
}
