export const IntroText = `A long time ago in a galaxy far, far away....
Dinorobot was fighting against unknown alien forces....
Nobody knows who will win....
You are the only one who can save the galaxy....
Ready?
Fight!!!`

export const Level1Completed = `Level 1 completed!
Congratulation Dinorobot! Alien forces have been defeated!
...Wait...
The aliens are coming back!
You must save the galaxy!
Ready?
Fight!!!`

export const Level2Completed = `Level 2 completed!
Congratulation Dinorobot! The Enemy has been defeated!
Now, the final battle!
Ready?
Here it comes...`


export const BossCompleted = `Thank you Dinorobot
The alien boss has been defeated!
You saved the Galaxy!`
