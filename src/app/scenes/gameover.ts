import * as Phaser from 'phaser'
import { RoundRectangle, TextBox } from 'phaser3-rex-plugins/templates/ui/ui-components'
import { Starfield } from 'src/app/background/starfield'


export class GameOver extends Phaser.Scene {
  starfield = <Starfield>{}
  textBox = <TextBox>{}

  constructor () {
    super('gameover')
  }

  preload() {
    this.load.setPath('assets/')
    this.load.image('starfield', 'backgrounds/starfield.jpg')
  }

  create(data: any) {
    this.starfield = new Starfield(this, 400, 300, 800, 600)

    const background = new RoundRectangle(this,0, 0, 0, 0, 0, 0x000000).setStrokeStyle(2, 0xffffff)
    this.add.existing(background)
    const text = this.add.bitmapText(0, 0, 'pixel', 'GAME OVER').setFontSize(60)
    this.textBox = new TextBox(this, {
      x: 0,
      y: 0,
      anchor: {
        centerX: '50%+0',
        centerY: '50%+0'
      },
      align: 'center',
      background,
      text,
      space: {
        left: 200,
        right: 200,
        top: 10,
        bottom: 20,
        icon: 0,
        text: 0,
      },
    }).setOrigin(0).layout().setInteractive()
    this.add.existing(this.textBox)

    this.add.text(350, 500, 'Restart!', {
      fontFamily: 'Arial',
      fontSize: '32px',
      stroke: '#ffffff',
    })
    .setInteractive().on('pointerdown', () => this.scene.start('intro') );
  }

  override update(time: number, delta: number) {
    this.starfield.update()
  }
}
