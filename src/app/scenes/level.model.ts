export interface Level extends Phaser.Scene {
  isLevelCompleted: boolean
  levelCompleted: () => void
}
