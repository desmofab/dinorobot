import * as Phaser from 'phaser'

export class ShipBullet extends Phaser.Physics.Arcade.Sprite {
  constructor(scene: Phaser.Scene, x: number, y: number) {
    super(scene, x, y, 'ship-bullet')
  }

  fire(x: number, y: number) {
    this.body.reset(x-7, y-40)
    this.setActive(true)
    this.setVisible(true)
    this.setVelocityY(-300)
  }

  override preUpdate(time: number, delta: number) {
    super.preUpdate(time, delta)
    if (this.y <= -32) {
      this.setActive(false)
      this.setVisible(false)
    }
  }
}

export class ShipBullets extends Phaser.Physics.Arcade.Group {
  constructor(scene: Phaser.Scene) {
    super(scene.physics.world, scene)
    this.createMultiple({
      classType: ShipBullet,
      frameQuantity: 5,
      key: 'ship-bullet',
      active: false,
      visible: false,
    })
  }

  fireBullet(x: number, y: number) {
    let bullet = this.get()
    if (bullet) {
      bullet.fire(x, y)
    }
  }
}
