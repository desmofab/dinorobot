import { Level } from '@scenes/level.model';
import { ShipBullets } from '@ship/bullet';
import Button from 'phaser3-rex-plugins/plugins/button.js';
import Flash from 'phaser3-rex-plugins/plugins/flash.js';
import VirtualJoystick from 'phaser3-rex-plugins/plugins/virtualjoystick.js';
import { HealthBar } from 'src/app/shared/healthbar';


const HealthbarOffsetX = -50
const HealthbarOffsetY = -30

export class Ship extends Phaser.Physics.Arcade.Sprite {
  level = <Level>{}
  cursorKeys = <Phaser.Types.Input.Keyboard.CursorKeys>{}
  afterBurner = <Phaser.GameObjects.Particles.ParticleEmitter>{}
  fireButton = <Button>{}
  fireButtonShape = <Phaser.GameObjects.Arc>{}
  fireBar = <Phaser.Input.Keyboard.Key>{}
  flashEffect = <Flash>{}
  bullets = <ShipBullets>{}
  beamSound = <Phaser.Sound.BaseSound>{}
  engineSound = <Phaser.Sound.BaseSound>{}
  joystick = <VirtualJoystick>{}
  life = <HealthBar>{}

  constructor(scene: Level, x: number, y: number) {
    super(scene, x, y, 'ship')
    this.level = scene
    scene.add.existing(this)
    scene.physics.add.existing(this).setScale(0.5).setCollideWorldBounds()
  }

  readyToPlay(): Ship {
    this.createJoyStick().createDefineKeys().createFireButton().createBullets().addSound().createAfterBurner().setHealth().setFlash()
    return this
  }

  setHealth(): Ship {
    this.life = new HealthBar(this.scene, this.x + HealthbarOffsetX, this.y + HealthbarOffsetY);
    return this
  }

  setFlash(): Ship {
    this.flashEffect = new Flash(this, {
      duration: 100,
      repeat: 10
    });
    return this
  }

  createAfterBurner(offset?: {x: number, y: number}): Ship {
    const particles = this.scene.add.particles('blu');
    this.afterBurner = particles.createEmitter({
        lifespan: 1000,
        speed: { min: 100, max: 200 },
        angle: 90,
        gravityY: 300,
        scale: { start: 0.5, end: 0 },
        blendMode: 'ADD',
        follow: this,
        followOffset: {x: offset?.x || -3, y: offset?.y || 60}
    });

    return this
  }

  createDefineKeys = (): Ship => {
    this.cursorKeys = this.scene.input.keyboard.createCursorKeys()
    this.fireBar = this.scene.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.SPACE)
    return this
  }


  createJoyStick(): Ship {
    this.joystick = new VirtualJoystick(this.scene, {
      x: 100,
      y: 500,
      radius: 50,
    });
    return this
  }

  removeJoystick(): Ship {
    this.joystick.setVisible(false)
    this.fireButton.destroy()
    this.fireButtonShape.setVisible(false)
    this.fireBar.destroy()
    return this
  }

  createFireButton(): Ship {
    // Spacebar
    this.fireBar.on('down', () => {
      this.bullets.fireBullet(this.x, this.y);
      this.beamSound.play()
    });

    // Touch Button
    this.fireButtonShape = this.scene.add.circle(700, 500, 50).setStrokeStyle(2, 0x1a65ac)
    this.fireButton = new Button(this.fireButtonShape);
    this.fireButton.on('click', () => {
      this.bullets.fireBullet(this.x, this.y)
      this.beamSound.play()
    });
    return this
  }

  createBullets(): Ship {
    this.bullets = new ShipBullets(this.scene);
    return this
  }

  addSound(): Ship {
    this.engineSound = this.scene.sound.add('audio_engine')
    this.engineSound.play({loop: true})
    this.beamSound = this.scene.sound.add('audio_laser')
    return this
  }

  override update(): Ship {
    if(this.level?.isLevelCompleted) {
      return this
    }
    this.body.velocity.setTo(0, 0)

    this.life.x = this.x + HealthbarOffsetX
    this.life.y = this.y + HealthbarOffsetY
    this.life.draw()

    if (this.joystick.left || this.cursorKeys.left.isDown) {
      this.body.velocity.x = -200
    }
    if (this.joystick.right || this.cursorKeys.right.isDown) {
      this.body.velocity.x = 200
    }
    if (this.joystick.up || this.cursorKeys.up.isDown) {
      if(this.y >= 400) {
        this.body.velocity.y = -200
      }
    }
    if (this.joystick.down || this.cursorKeys.down.isDown) {
      this.body.velocity.y = 200
    }
    return this
  }

  hit() {
    this.setVelocity(0,0) // Prevent Ship from being pushed when hit by enemy bullets
    this.scene.sound.play('ship-hit')
    this.life.decrease(Phaser.Math.Between(10, 20))
    if (this.life.life <= 0) {
      this.destroyShip()
      return
    }

    this.flashEffect.flash()
  }

  destroyShip() {
    this.body.enable = false
    this.life.destroy()
    this.afterBurner.setVisible(false)
    this.setScale(2)
    this.on('animationcomplete', () => this.scene.scene.start('gameover'), this)
    this.anims.play('kaboom2')
    this.scene.sound.play('ship_explosion')
  }
}
