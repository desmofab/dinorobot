import { Ship } from '@ship/ship'
import * as Phaser from 'phaser'

export class Eye1Bullet extends Phaser.Physics.Arcade.Sprite {
  constructor(scene: Phaser.Scene, x: number, y: number) {
    super(scene, x, y, 'eye1-bullet')

    this.anims.create({
      key: 'eye1-bullet',
      duration: 500,
      frames: this.anims.generateFrameNames('eye1-bullet'),
      repeat: -1,
      yoyo: true,
      showOnStart: true
    })
  }

  fire(target: Ship, x: number, y: number) {
    this.body.reset(x, y+40)
    this.setActive(true).setScale(Phaser.Math.Between(1,1.5)).setVisible(true).play('eye1-bullet')
    this.scene.physics.moveTo(this, target.x, target.y, Phaser.Math.Between(80, 320))
  }

  override preUpdate(time: number, delta: number) {
    super.preUpdate(time, delta)
    if (this.y <= -32) {
      this.setActive(false)
      this.setVisible(false)
    }
  }
}

export class Eye1Bullets extends Phaser.Physics.Arcade.Group {
  constructor(scene: Phaser.Scene) {
    super(scene.physics.world, scene)
    this.createMultiple({
      classType: Eye1Bullet,
      frameQuantity: 5,
      key: 'eye1-bullet',
      active: false,
      visible: false,
    })
  }

  fireBullet(target: Ship, x: number, y: number) {
    let bullet = this.get()
    if (bullet) {
      bullet.fire(target, x, y)
    }
  }
}
