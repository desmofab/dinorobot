import { Eye1Bullets } from '@bads/eyes1/eye1-bullet';
import { Level } from '@scenes/level.model';
import { Ship } from '@ship/ship';
import { HealthBar } from 'src/app/shared/healthbar';

const HealthbarOffsetX = -50
const HealthbarOffsetY = -30

export class Eye1 extends Phaser.Physics.Arcade.Sprite {
  level = <Level>{}
  life = <HealthBar>{}

  constructor(scene: Level, x: number, y: number, texture: string) {
    super(scene, x, y, texture)
    this.level = scene
    this.setHealth()
  }

  setHealth(): Eye1 {
    this.life = new HealthBar(this.scene, this.x + HealthbarOffsetX, this.y + HealthbarOffsetY);
    return this
  }

  override preUpdate(time: number, delta: number) {
    super.preUpdate(time, delta)
    this.life.x = this.x + HealthbarOffsetX
    this.life.y = this.y + HealthbarOffsetY
    this.life.draw()
  }

  hit() {
    this.setVelocity(0,0) // Prevent alien from being pushed when hit by bullets
    this.scene.sound.play('audio_hit')
    this.life.decrease(Phaser.Math.Between(10, 15))
    if (this.life.life <= 0) {
      this.destroyEye1()
      return
    }

    const startColor = Phaser.Display.Color.ValueToColor(0xffffff)
		const endColor = Phaser.Display.Color.ValueToColor(0xff0f00)
    this.scene.tweens.addCounter({
			from: 0,
			to: 50,
			duration: 50,
			repeat: 5,
			yoyo: true,
			ease: Phaser.Math.Easing.Sine.InOut,
			onUpdate: tween => {
				const value = tween.getValue()
				const colorObject = Phaser.Display.Color.Interpolate.ColorWithColor(
					startColor,
					endColor,
					50,
					value
				)

				const color = Phaser.Display.Color.GetColor(
					colorObject.r,
					colorObject.g,
					colorObject.b
				)

				this.setTint(color)
			}
		})
  }

  destroyEye1() {
    this.body.enable = false
    this.life.destroy()
    this.setScale(1)
    this.on('animationcomplete', () => { // Doesn't work if preUpdate is overridden
      this.destroy()
      this.level.levelCompleted()
    }, this)
    this.anims.play('kaboom')
    this.scene.sound.play('audio_explosion')

  }
}

const enemy1Cfg: Phaser.Types.GameObjects.Group.GroupCreateConfig | Phaser.Types.Physics.Arcade.PhysicsGroupConfig = {
  classType: Eye1,
  active: true,
  key: 'bad1',
  visible: true,
  repeat: 0,
  quantity: 4,
  collideWorldBounds: true,
  runChildUpdate: true,
  setXY: {
      x:400,
      y:100,
      stepX:50,
      stepY:50
  },
  setScale: {
    x:0.1,
    y:0.1,
    stepX:0.1,
    stepY:0.1
  }
}

export class Eyes1 extends Phaser.Physics.Arcade.Group {
  alienMove = 0
  bullets = <Eye1Bullets>{}

  constructor(scene: Phaser.Scene) {
    super(scene.physics.world, scene)
    const ellipse = new Phaser.Geom.Ellipse(400, 100, 500, 200)
    this.createMultiple(enemy1Cfg)
    Phaser.Actions.PlaceOnEllipse(this.getChildren(), ellipse)
    this.createBullets()
  }

  createBullets(): Eyes1 {
    this.bullets = new Eye1Bullets(this.scene);
    return this
  }

  fireOrder(target: Ship): Eyes1 {
    const timer = this.scene.time.addEvent({
      delay: Phaser.Math.Between(1000, 3000),
      callback: this.fireBullet,
      args: [target],
      callbackScope: this,
      loop: true
  });
    return this
  }

  fireBullet(target: Ship) {
    const eye1 = <Eye1>this.getChildren()[Phaser.Math.Between(0, this.getChildren().length - 1)]
    if (eye1) {
      this.bullets.fireBullet(target, eye1.x, eye1.y)
    }
  }

  update() {
    Phaser.Actions.IncX(this.getChildren(), Math.cos(this.alienMove))
    Phaser.Actions.IncY(this.getChildren(), Math.sin(this.alienMove))
    this.alienMove += 0.02
  }
}
